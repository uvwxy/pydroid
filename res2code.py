#!/usr/bin/python

# This script is used to create the Java code needed to have runtime GUI object access for Android.
# TODO: options what to generate: declaration vs initialization, and: which types TextViews a.s.o.

import StringIO
import sys

from lxml import etree
from optparse import OptionParser

parser = OptionParser()
parser.add_option("-f", "--resfile", dest='resFile', default=None, help="Resource input file (.xml)")
(options,args) = parser.parse_args()

if options.resFile == None:
    print "Please provide a file"
    sys.exit()

fileObject = open(options.resFile)
fileContents = fileObject.read()
fileAsString = StringIO.StringIO("" + fileContents)

tree = etree.parse(fileAsString)

#for element in tree.iter():
#    objType = element.tag
#    print("%s - %s" %  (element.tag, element.text))
#    for c in element:
#        for d in c:
#            print d.tag
allElements =  tree.findall('.//*')       

declLines = ""
initLines = ""

for x in allElements:
    objType =  x.tag
    objID = x.get("{http://schemas.android.com/apk/res/android}id")
    # print all objects that have a defined ID!
    if objID != None:
        objID = str(objID).split('/')[1]
        declLines+="private %s %s = null;" % (objType, objID) + "\n"
        initLines+="\t%s = (%s) findViewById(R.id.%s);" % (objID, objType, objID) + "\n"
        
print declLines
print "private void initGUI(){"
print initLines + "}"
sys.exit()
